package com.seleniumexpress.sm.service;

import com.seleniumexpress.sm.model.Student;
import java.util.List;

public interface StudentAppService {

  List<Student> getAllStudents();

}
