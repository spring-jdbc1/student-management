package com.seleniumexpress.sm.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Student {

  private int id;

  private String name;

  private long mobile;

  private String country;
}
