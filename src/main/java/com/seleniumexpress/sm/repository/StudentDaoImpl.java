package com.seleniumexpress.sm.repository;

import com.seleniumexpress.sm.dao.StudentDao;
import com.seleniumexpress.sm.model.Student;
import com.seleniumexpress.sm.rowmapper.StudentRowMapper;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * in build component
 */
@Repository
@RequiredArgsConstructor
public class StudentDaoImpl implements StudentDao {

  private final JdbcTemplate jdbcTemplate;

  @Override
  public List<Student> loadStudents() {

    String sql = "SELECT * FROM seleniumexpress.students";

    return jdbcTemplate.query(sql, new StudentRowMapper());
  }

}
