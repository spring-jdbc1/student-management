package com.seleniumexpress.sm.rowmapper;

import com.seleniumexpress.sm.model.Student;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.tree.TreePath;
import org.springframework.jdbc.core.RowMapper;

public class StudentRowMapper implements RowMapper<Student> {

  @Override
  public Student mapRow(ResultSet resultSet, int rowNum) throws SQLException {

    int id = resultSet.getInt("ID");
    String name = resultSet.getString("NAME");
    long mobile = resultSet.getLong("MOBILE");
    String country = resultSet.getString("COUNTRY");

    return Student.builder().id(id).name(name).mobile(mobile).country(country).build();

  }
}
