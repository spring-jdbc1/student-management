package com.seleniumexpress.sm.business;

import com.seleniumexpress.sm.dao.StudentDao;
import com.seleniumexpress.sm.model.Student;
import com.seleniumexpress.sm.service.StudentAppService;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Data
@Service
public class StudentAppBusiness implements StudentAppService {

  private final StudentDao studentDao;

  @Override
  public List<Student> getAllStudents() {
    final List<Student> studentList = studentDao.loadStudents();

    studentList.forEach(System.out::println);
    return studentList;
  }
}
