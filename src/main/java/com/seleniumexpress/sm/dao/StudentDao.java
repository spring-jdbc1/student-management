package com.seleniumexpress.sm.dao;

import com.seleniumexpress.sm.model.Student;
import java.util.List;

public interface StudentDao {

  List<Student> loadStudents();

}
