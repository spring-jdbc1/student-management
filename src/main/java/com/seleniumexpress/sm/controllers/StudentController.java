package com.seleniumexpress.sm.controllers;

import com.seleniumexpress.sm.dao.StudentDao;
import com.seleniumexpress.sm.model.Student;
import com.seleniumexpress.sm.service.StudentAppService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class StudentController {

  private final StudentAppService studentAppService;

  @GetMapping(value = "/showStudent")
  public String showStudentList(Model model) {

    final List<Student> students = studentAppService.getAllStudents();

    model.addAttribute("students", students);

    return "student-list";
  }

}
