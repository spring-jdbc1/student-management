<%--
  Created by IntelliJ IDEA.
  User: JesúsCruzHernández
  Date: 1/29/2024
  Time: 11:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"
         isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <style>

      table, th, td {
        border: 1px solid green;
      }

      div {
        display: flex;
        justify-content: center; /* Horizontally center */
      }

      .center-text {
        text-align: center;
      }

    </style>
</head>
<body>
<h1 class="center-text">Selenium express Jdbc course</h1>
<div>
    <table>
        <thead>
        <tr>
            <td>id</td>
            <td>name</td>
            <td>mobile</td>
            <td>country</td>
        </tr>
        </thead>
        <c:forEach var="student" items="${students}">
            <tr>
                <td>${student.id}</td>
                <td>${student.name} </td>
                <td> ${student.mobile} </td>
                <td> ${student.country} </td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>
